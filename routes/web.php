<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\CriticController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard.index');
});

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('guest');

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/create', [CastController::class, 'create'])->middleware('auth');
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{id}', [CastController::class, 'show']);
Route::get('/cast/{id}/edit', [CastController::class, 'edit'])->middleware('auth');
Route::put('/cast/{id}', [CastController::class, 'update']);
Route::delete('/cast/{id}', [CastController::class, 'destroy'])->middleware('auth');

Route::get('/genre', [GenreController::class, 'index']);
Route::get('/genre/create', [GenreController::class, 'create'])->middleware('auth');
Route::post('/genre', [GenreController::class, 'store']);
Route::get('/genre/{id}', [GenreController::class, 'show']);
Route::get('/genre/{id}/edit', [GenreController::class, 'edit'])->middleware('auth');
Route::put('/genre/{id}', [GenreController::class, 'update']);
Route::delete('/genre/{id}', [GenreController::class, 'destroy'])->middleware('auth');

Route::get('/film', [FilmController::class, 'index']);
Route::get('/film/create', [FilmController::class, 'create'])->middleware('auth');
Route::post('/film', [FilmController::class, 'store']);
Route::get('/film/{id}', [FilmController::class, 'show']);
Route::get('/film/{id}/edit', [FilmController::class, 'edit'])->middleware('auth');
Route::put('/film/{id}', [FilmController::class, 'update']);
Route::delete('/film/{id}', [FilmController::class, 'destroy'])->middleware('auth');

Route::get('/critic', [CriticController::class, 'index']);
Route::get('/critic/create', [CriticController::class, 'create'])->middleware('auth');
Route::post('/critic', [CriticController::class, 'store']);
Route::get('/critic/{id}', [CriticController::class, 'show']);
Route::get('/critic/{id}/edit', [CriticController::class, 'edit'])->middleware('auth');
Route::put('/critic/{id}', [CriticController::class, 'update']);
Route::delete('/critic/{id}', [CriticController::class, 'destroy'])->middleware('auth');

// Route::get('/profile', [ProfileController::class, 'index'])->middleware('auth');
// Route::get('/profile/{id}/edit', [ProfileController::class, 'edit'])->middleware('auth');
// Route::put('/profile/{id}', [ProfileController::class, 'update']);