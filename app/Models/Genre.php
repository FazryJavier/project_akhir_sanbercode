<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;
    protected $table='genres';
    protected $primaryKey = 'id';
    protected $guarded=[];
    public $timestamps = true;

    public function film() {
        return $this->belongsToMany(Film::class, 'genre_film', 'genre_id', 'film_id');
    }
}
