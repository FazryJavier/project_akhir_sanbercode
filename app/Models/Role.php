<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table='roles';
    protected $primaryKey = 'id';
    protected $guarded=[];
    public $timestamps = true;

    public function cast() {
        return $this->belongsTo(Cast::class, 'cast_id');
    }

    public function film() {
        return $this->belongsTo(Film::class, 'film_id');
    }
}
