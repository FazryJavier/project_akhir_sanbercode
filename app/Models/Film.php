<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;
    protected $table='films';
    protected $primaryKey = 'id';
    protected $guarded=[];
    public $timestamps = true;

    public function role() {
        return $this->hasMany(Role::class, 'film_id');
    }

    public function critic() {
        return $this->hasMany(Critic::class, 'film_id');
    }

    public function genre() {
        return $this->belongsToMany(Genre::class, 'genre_film', 'genre_id', 'film_id');
    }
}
