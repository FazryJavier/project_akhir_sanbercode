<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Critic;

class CriticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $critic = Critic::all();
        return view('critic.table', compact('critic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('critic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'content'=>'required',
            'point'=>'required',
        ]);
        
        $critic = new Critic;
        $critic->content = $request->content;
        $critic->point = $request->point;
        $critic->save();

        return redirect('/film') -> with('success','Comment created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $criticShow = Critic::find($id);

        return view('critic.show', compact('criticShow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $criticEdit=Critic::where('id', $id)->firstorfail();

        return view('critic.edit', compact('criticEdit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $criticEdit=Critic::where('id', $id)->update([
            'content'=>$request['content'],
            'point'=>$request['point'],
        ]);
        
        return redirect('/film')  -> with('success','Data Cast updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $critic = Critic::find($id);

        $critic -> delete();

        return redirect('/film');
    }
}
