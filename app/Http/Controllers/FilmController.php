<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.table', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('film.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request -> validate([
            'judul'=>'required',
            'ringkasan'=>'required',
            'tahun'=>'required',
            'poster'=>'image|file|max:2048',
        ]);

        if($request->file('poster')) {
            $validatedData['poster'] = $request->file('poster')->store('film-image');
        }

        $genre_arr = explode(',', $request["genre"]);

        $genre_ids = [];
        foreach($genre_arr as $genre_name) {
            $genre = Genre::where('nama', $genre_name)->first();
            if($genre) {
                $genre_ids[] = $genre -> id;
            } else {
                $new_genre = Genre::create(["nama" => $genre_name]);
                $genre_ids[] = $new_genre->id;
            }
        }

        Film::create($validatedData);
        $genre->genre()->sync($genre_ids);

        return redirect('/film') -> with('success','Data Film created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $filmShow = Film::find($id);

        return view('film.show', compact('filmShow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $filmEdit=Film::where('id', $id)->firstorfail();

        return view('film.edit', compact('filmEdit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = [
            'judul'=>'required',
            'ringkasan'=>'required',
            'tahun'=>'required',
            'poster'=>'image|file|max:2048',
        ];
        // $validatedData = $filmEdit=Film::where('id', $id)->update([
        //     'judul'=>'required',
        //     'ringkasan'=>'required',
        //     'tahun'=>'required',
        //     'poster'=>'image|file|max:2048',
        // ]);

        if($request->file('poster')) {
            $image['poster'] = $request->file('poster')->store('film-image');
        }

        $validatedData = $request->validate($image);
        
        Film::where('id', $id)
            ->update($validatedData);

        return redirect('/film')  -> with('success','Data Film updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        $film -> delete();

        return redirect('/film');
    }
}
