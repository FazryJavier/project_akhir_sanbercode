@extends('layouts/master')

@section('judul')
Detail Film
@endsection

@section('content')
    <h1>Review {{$filmShow->judul}} <br></h1>
    <h4>Tahun : {{$filmShow->tahun}}</h4>
    <h4>Ringkasan : {{$filmShow->ringkasan}}</h4><br>
    <h4>Poster : </h4>
    <img src="{{asset('storage/' . $filmShow->poster)}}" alt="" class="img-fluid mt-3"><br><br>
    <div>
        Genre : 
        @forelse($filmShow->genre as $genre)
        <button class="btn btn-primary btn-sm">{{$genre->nama}}</button>
        @empty
        No Genre
        @endforelse
    </div>        
    <br><br>
    <a href="/film" type="button" class="btn btn-secondary">Back</a>
    <a href="/critic/create" type="button" class="btn btn-primary">Give a Comment</a>
@endsection