@extends('layouts/master')

@section('judul')
Edit Data Film
@endsection

@section('content')
<form method="POST" action="{{url('/film/'.$filmEdit->id)}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
        <div class="mb-3">
            <label for="judul" class="form-label">Judul</label>
            <input type="text" value="{{$filmEdit->judul}}" name="judul" class="form-control">
        </div> 
        @error('judul')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="mb-3">
            <label for="tahun" class="form-label">Tahun</label>
            <input type="text" value="{{$filmEdit->tahun}}" name="tahun" class="form-control">
        </div> 
        @error('tahun')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="mb-3">
            <label for="ringkasan" class="form-label">Ringkasan</label>
            <textarea name="ringkasan"  class="form-control" rows="5">{{$filmEdit->ringkasan}}</textarea>
        </div>
        @error('ringkasan')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="mb-3">
            <label for="poster" class="form-label">Upload Poster</label>
            @if($filmEdit -> poster)
                <img src="{{asset('storage/' . $filmEdit->poster)}}" alt="" class="img-preview img-fluid mb-3 d-block">
            @else
                <img class="img-preview img-fluid">
            @endif
            <input type="file" class="form-control" id="poster" name="poster" onchange="previewImage()">
        </div>
        @error('poster')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <a href="/film" type="button" class="btn btn-secondary">Back</a>
        <button type="submit" class="btn btn-primary  ml-3">Update Data</button>
    </form>  

    <script>
        function previewImage() {
            const image = document.querySelector('#poster');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endsection