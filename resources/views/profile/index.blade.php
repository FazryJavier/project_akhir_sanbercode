@extends('layouts/master')

@section('judul')
Profile
@endsection

@section('content')
    <h1>SanberBook</h1>
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    <br>
    <h3>Tampilan Tabel Cast</h3>
    <ol>
        <li>Untuk melihat data tabel tekan <a href="">Link ini</a></li>
        <li>Untuk tambah data tabel tekan <a href="">Link ini</a></li>
    </ol>
    <a href="/dashboard" type="button" class="btn btn-secondary">Kembali</a>
@endsection