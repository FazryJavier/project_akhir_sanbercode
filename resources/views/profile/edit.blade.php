@extends('layouts/master')

@section('judul')
Edit Profile
@endsection

@section('content')
<form method="POST" action="{{url('/profile/'.$profileEdit->id)}}">
    @csrf
    @method('PUT')
        <div class="mb-3">
            <label for="nama" class="form-control" readonly>Nama</label>
            <input type="text" value="{{Auth::user()->name}}" name="nama" class="form-control">
        </div> 
        <div class="mb-3">
            <label for="umur" class="form-label">Umur</label>
            <input type="text" value="{{$profileEdit->umur}}" name="umur" class="form-control">
        </div> 
        @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="mb-3">
            <label for="bio" class="form-label">Bio</label>
            <textarea name="bio"  class="form-control" rows="3">{{$profileEdit->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="mb-3">
            <label for="alamat" class="form-label">Alamat</label>
            <textarea name="alamat"  class="form-control" rows="3">{{$profileEdit->alamat}}</textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <a href="/cast" type="button" class="btn btn-secondary">Kembali</a>
        <button type="submit" class="btn btn-primary  ml-3">Update Data</button>
    </form>  
@endsection